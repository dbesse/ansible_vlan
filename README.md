# ansible_vlan

A simple playbook to test the ansible ios_vlan module.

* VLAN 100 is created
* An interface is added to it
* A check is performed to make sure the interface is part of the VLAN
* The VLAN is deleted

For simplicity the default username/password are in plain text
